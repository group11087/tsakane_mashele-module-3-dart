import 'package:flutter/material.dart';

class RegisterForm extends StatefulWidget {
  const RegisterForm({Key? key}) : super(key: key);

  @override
  State<RegisterForm> createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  TextEditingController emailCont = TextEditingController();
  TextEditingController pwdCont = TextEditingController();
  TextEditingController nameCont = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Register'),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 18.0, vertical: 30),
        child: Form(
          key: formKey,
          child: ListView(
            children: [
              const Text('Register',
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Colors.teal,
                  )),
              const SizedBox(
                height: 20,
              ),
              TextFormField(
                controller: emailCont,
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please enter the email';
                  }
                },
                decoration: const InputDecoration(
                  labelText: 'Enter email',
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              TextFormField(
                obscureText: true,
                controller: pwdCont,
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please enter the password';
                  }
                },
                decoration: const InputDecoration(
                  labelText: 'Enter password',
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              TextFormField(
                obscureText: true,
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please confirm the password';
                  }
                  if (value != pwdCont.text) {
                    return 'Password and confirm password not matching';
                  }
                },
                decoration: const InputDecoration(
                  labelText: 'Confirm password',
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              TextFormField(
                controller: nameCont,
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please enter the name';
                  }
                },
                decoration: const InputDecoration(
                  labelText: 'Enter name',
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              MaterialButton(
                onPressed: () {
                  if (formKey.currentState!.validate()) {
                    const RegisterForm();
                  }
                },
                height: 50,
                minWidth: 100,
                color: Colors.teal,
                child: const Text('Go',
                    style: TextStyle(color: Colors.white, fontSize: 20)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
