import 'package:flutter/material.dart';

import 'RegisterForm.dart';

class loginregister extends StatefulWidget {
  const loginregister({Key? key}) : super(key: key);

  @override
  State<loginregister> createState() => _loginregisterState();
}

TextEditingController emailCont = TextEditingController();
TextEditingController pwdCont = TextEditingController();
GlobalKey<FormState> formKey = GlobalKey<FormState>();

class _loginregisterState extends State<loginregister> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Mtn Module 3'),
          centerTitle: true,
        ),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 18.0, vertical: 30),
            child: Form(
              key: formKey,
              child: ListView(
                children: [
                  const Text('Login',
                      style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                        color: Colors.teal,
                      )),
                  const SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please enter email address';
                      }
                    },
                    controller: emailCont,
                    decoration: const InputDecoration(
                      labelText: 'Enter email',
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    obscureText: true,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please enter the password';
                      }
                    },
                    controller: pwdCont,
                    decoration: const InputDecoration(
                      labelText: 'Enter password',
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  MaterialButton(
                    onPressed: () {
                      if (formKey.currentState!.validate()) {
                        loginregister();
                      }
                    },
                    height: 50,
                    minWidth: 100,
                    color: Colors.teal,
                    child: const Text('Submit',
                        style: TextStyle(color: Colors.white, fontSize: 20)),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  OutlinedButton(
                    child: const Text('Register'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const RegisterForm()),
                      );
                    },
                  )
                ],
              ),
            ),
          ),
        ));
  }
}
