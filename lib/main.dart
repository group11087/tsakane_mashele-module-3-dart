import 'package:flutter/material.dart';
import 'profile_page.dart';
import 'loginregister.dart';
import 'dashboard.dart';
import 'edit_profile_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          primarySwatch: Colors.red,
        ),
        title: "Mtn modue 3",
        home: Scaffold(
          appBar: AppBar(title: const Text("Mtn Module 3")),
        ));
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return const loginregister();
  }
}
